import time
import threading

import cv2
import numpy as np
from cmd2 import Cmd

import pyglet
from pyglet.gl import *
from pyglet.gl.glu import *
from pyglet.window import *

import cvhelper
from cvhelper.image_provider import *
from my_settings import *

# ===================== Image Processor =====================
class ImageProcessor(object):
    def __init__(self):
        self.thread = threading.Thread(target=self.update)
        self.image_ready_event = threading.Event()
        self.src_image = None
        self.processed_image = None

    def start(self):
        self.is_running = True
        self.thread.start()
    
    def stop(self):
        self.is_running = False
        self.image_ready_event.set()
        self.thread.join()
        # print("stopping the ImageProcessor ...")

    def set_source_image(self, new_image):
        self.src_image = np.copy(new_image)
        if (not self.src_image is None):
            self.image_ready_event.set()

    def get_processed_image(self):
        return None if (self.processed_image is None) else np.copy(self.processed_image)
        # return self.src_image

    def process_image(self, src_image):
        demo_image = np.copy(src_image)
        # Processing the image
        dest_image = cv2.medianBlur(src_image, 5)
        # dest_image = cv2.cvtColor(dest_image, cv2.COLOR_GRAY2BGR)
        dest_image = cv2.cvtColor(dest_image, cv2.COLOR_BGR2GRAY)

        # Run the detection algorithm
        circles = cv2.HoughCircles(dest_image,
            cv2.HOUGH_GRADIENT,
            dp=1,
            minDist=40,
            param1=50,
            param2=30,
            minRadius=20,
            maxRadius=0)

        if (not circles is None):
            circles = np.uint16(np.around(circles))
            for i in circles[0,:]:
                # draw the outer circle
                cv2.circle(demo_image,(i[0],i[1]),i[2],(0,255,0),2)
                # draw the center of the circle
                cv2.circle(demo_image,(i[0],i[1]),2,(0,0,255),3)

        return demo_image

    def update(self):
        while (self.is_running):
            self.image_ready_event.wait()
            self.image_ready_event.clear()

            if (self.src_image is None):
                continue

            self.processed_image = self.process_image(self.src_image)
            # print('Processing image ...')

# ===================== CLI =====================
class CommandHandler(Cmd):
    """Class to handle shell command"""

    prompt = "cmd> "
    intro = "Computer vision CLI"

    def __init__(self, app):
        super(CommandHandler, self).__init__()

        self.app = app

    # Commands

    def do_test(self, arg):
        """test command"""
        print('This is a test command')

    def do_stop(self, arg):
        self.app.stop()
        return self._STOP_AND_EXIT

    def do_quit(self, arg):
        self.app.stop()
        return super(CommandHandler, self).do_quit(arg)

    # Internal functions

# ===================== Window =====================
class MainWindow(pyglet.window.Window):
    def __init__(self, *args, **kwargs):
        # print('args: {}'.format(args))
        # print('kwargs: {}'.format(kwargs))
        super(MainWindow, self).__init__(**kwargs)

        self.is_dirty = False

    def set_up(self):
        glClearColor(0, 0, 0, 1)
        self.bg_image = None
        # self.bg_image = pyglet.image.ImageData(512, 512, "", None)
        # self.label = pyglet.text.Label('Main window', 
        #                 font_name='Times New Roman', 
        #                 font_size=36,
        #                 x=10, y=10)

    def mark_dirty(self):
        self.is_dirty = True

    def on_draw(self):
        if (not self.is_dirty):
            return

        self.clear()
        # self.label.draw()
        # print('on_draw...')
        if (not self.bg_image is None):
            # print('drawing background image ...')
            self.bg_image.blit(0, 0)

        self.is_dirty = False

    def set_background_image(self, new_image):
        width, height, channel = new_image.shape

        raw_image_data = new_image.flatten()
        image_byte_size = raw_image_data.shape[0]
        # print('set_background_image: {} - shape: {}'.format(new_image, new_image.shape))
        # print('set_background_image - width: {} - height: {} - channel: {}'.format(width, height, channel))
        image_data = (GLubyte * image_byte_size)( *raw_image_data.astype('uint8') )
        # image_texture = (GLubyte* image_byte_size)( *new_image.astype('uint8') )
        self.bg_image = pyglet.image.ImageData(
            width, height, "BGR", image_data, width * 3)

    def take_screenshot(self):
        screen_image = pyglet.image.get_buffer_manager().get_color_buffer()
        screen_image.save(SCREENSHOT_FILE_PATH)

    def on_key_press(self, symbol, modifiers):
        super(MainWindow, self).on_key_press(symbol, modifiers)

        if (symbol == key.F9):
            self.take_screenshot()    

# ===================== Application  =====================
class Application(object):
    def __init__(self):
        self.img_processor = ImageProcessor()
        self.cli = CommandHandler(self)

        self.ipv_webcam = ImageProviderWebcam(0)
        self.ipv_dir = ImageProviderDirectory('./data',
            [
                "*.png",
                "*.jpg",
                # "*.gif"
            ])
        # ipv_dir = ImageProviderDirectory('./data', "*.*")

        # ipv = self.ipv_webcam
        self.ipv = self.ipv_dir

        self.is_running = False
        self.thread = threading.Thread(target=self.event_loop)

        self.main_window = MainWindow(
                        width=512, height=512,
                        caption='Main Window')
        self.main_window.set_up()

    def start(self):
        self.img_processor.start()
        self.ipv.start()

        self.is_running = True
        self.thread.start()

        self.cli_thread = threading.Thread(target=self.cli.cmdloop)
        self.cli_thread.start()

        pyglet.app.run()
        # self.gui_thread = threading.Thread(target=pyglet.app.run)
        # self.gui_thread.start()

        # self.cli.cmdloop()

    def stop(self):
        # print("stopping the app ...")
        self.is_running = False
        self.thread.join()

        self.main_window.close()
        # self.gui_thread._stop()
        # self.gui_thread.join()

        self.img_processor.stop()
        self.ipv.stop()
        cv2.destroyAllWindows()

        # self.cli_thread.join()

        # print("stop the app ...")

    def update(self):
        # Get image from the image provider
        src_image = self.ipv.get_image()
        if (src_image is None):
            return
        
        self.img_processor.set_source_image(src_image)

        # print("event_loop ...")
        processed_image = self.img_processor.get_processed_image()
        # print("processed_image: {}".format(processed_image))

        # Processing the image
        if (not processed_image is None):
            self.main_window.set_background_image(processed_image)

            # cv2.imshow("Image viewer", processed_image)
            
            # check_key = ord('q')
            # input_key = cv2.waitKey(1)
            # if (input_key > 0):
            #     print('input_key: {} - check_key: {}'.format(input_key, check_key))
            #     key = input_key & check_key
            #     if (not key == 0):
            #         print('key: {}'.format(key))
            #         self.stop()
            #         return
        
        # HACK
        time.sleep(1.0)

    def event_loop(self):
        while (self.is_running):
            try:
                self.update()
            except Exception as e:
                # print("Exception: {}".format(traceback.format_exc()))
                print("Exception: {}".format(e))

# ===================== Main =====================
def main():
    # DEFAULT_CONFIG_FILE = "./config/config.ini"
    # config = SettingBase.create_from_config_file(DEFAULT_CONFIG_FILE, "Test")

    app = Application()
    app.start()

if __name__ == "__main__":
    main()