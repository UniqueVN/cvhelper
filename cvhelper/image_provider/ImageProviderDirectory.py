from os import listdir, path
import fnmatch

import cv2

from .ImageProviderBase import *

class ImageProviderDirectory(ImageProviderBase):
    """This class provide image inside a specified directory"""

    def __init__(self, image_dir, filename_filters=["*.*"], recursive = True):
        self.image_dir = image_dir
        self.filename_filters = filename_filters
        self.recursive = recursive

        # NOTE: We only scan the folder once. May need to refresh?
        self.scan_dir()
        
    def scan_dir(self):
        self.all_image_paths = []

        # TODO: Instead of keeping a list of all images, just use the iterator?
        # When there are a lot of images in the directory, this loop can be slow
        for filename in listdir(self.image_dir):
            check_file_path = path.join(self.image_dir, filename)
            if (path.isfile(check_file_path) and
                any(fnmatch.fnmatch(filename, filter) for filter in self.filename_filters)):
                self.all_image_paths.append(check_file_path)
            # TODO: Need to scan sub directory if recursive is True
        
        self.total_image_count = len(self.all_image_paths)
        self.next_image_index = min(0, self.total_image_count - 1)

        print(self.total_image_count)

    def get_image(self):
        if (self.total_image_count <= 0):
            return None

        if (self.next_image_index >= self.total_image_count):
            # TODO: Go back to the beginning?
            self.next_image_index = 0
            # return None
        
        next_image_file_path = self.all_image_paths[self.next_image_index]
        self.next_image_index += 1
        
        next_image = cv2.imread(next_image_file_path)
        return next_image
