import threading
from threading import Thread

import cv2

from .ImageProviderBase import *

class ImageProviderWebcam(ImageProviderBase):
    """This class capture the images from the webcam"""
    
    def __init__(self, webcam_index = 0):
        self.webcam_index = webcam_index
        self.webcam = cv2.VideoCapture(self.webcam_index)
        # NOTE: right now only keep the latest image from the webcam stream
        # We can add support for cached image buffer
        self._image = None
        
        # Use a separated thread to capture the image from the webcam
        self.is_running = False
        self.thread = Thread(target=self.update, args=())

    def get_image(self):
        return self._image

    def start(self):
        self.is_running = True
        self.thread.start()
    
    def stop(self):
        self.is_running = False
        self.thread.join()
    
    def update(self):
        while (self.is_running):
            ret, self._image = self.webcam.read()
            # TODO: Add support for maximum image capture fps