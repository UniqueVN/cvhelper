class ImageProviderBase(object):
    """This class provide the interface for all the object that provide source image 
    to image processing tasks"""
    
    def __init__(self):
        self._name = 'ImageProvider'

    def get_image(self):
        raise NotImplementedError( "Must implement this function in child class" )

    def start(self):
        # To be implemented by the child provider
        return

    def stop(self):
        # To be implemented by the child provider
        return
