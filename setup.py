from setuptools import Command, find_packages, setup

__version__ = "0.0.1"
setup(
    name = "cvhelper",
    version = __version__,
    description = "Collection of Python scripts for Computer Vision",
    long_description = "A collection of Python scripts to help working with Computer Vision project easier",
    url = "https://bitbucket.org/UniqueVN/cvhelper",
    author = "Thang To",
    author_email = "uniquevn@gmail.com",
    license = "MIT",
    classifiers = [
        "Intended Audience :: Developers",
        "Topic :: Utilities",
        "License :: MIT",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.6",
    ],
    keywords = "cvhelper",
    packages=['cvhelper'],
    # packages = find_packages('my_settings', 'test'),
    # package_dir = {'': 'my_settings'},
    package_data = {
        # TODO
    },
    install_requires = [
        "opencv-python",
        "my_settings"
    ],
    dependency_links = [
        "https://bitbucket.org/UniqueVN/mysettings/get/e4e327ed46ce.zip",
    ],
    extras_require = {
        # "test": [ "pytest" ]
    },
    entry_points = {
        "console_scripts": [
            "test_cv=cvhelper.test.test_cv:main",
        ]
    },
    # cmdclass = { 
    #     "test": 
    # }
    scripts=[],
)